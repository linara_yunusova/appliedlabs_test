<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Helper\Handler;

spl_autoload_register(function ($className) {

    $classPath = str_replace("\\", DIRECTORY_SEPARATOR, $className);
    $classPath = join('/', array_map('lcfirst', explode('/', $classPath)));
    $classPath = strtolower(preg_replace('/[A-Z]/', '_$0', $classPath));
    $classPath = ltrim($classPath, '/');
    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/core/' . $classPath . '.php')) {
        include_once $_SERVER['DOCUMENT_ROOT'] . '/core/' . $classPath . '.php';
    }
});


$handler_parser = new Handler();
$handler = $handler_parser->setHandler();
$result = $handler->action();


return Helper\Response::response($result);
exit();

