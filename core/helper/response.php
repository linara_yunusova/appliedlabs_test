<?php
namespace Helper;


class Response
{
    const DEFAULT_STATUS_MESSAGES = [
        400 => "Wrong request.",
        401 => "Authorization error.",
        404 => "Not found data.",
        500 => "System errror. Try again."
    ];

    public function response($result)
    {
        header('Content-Type: application/json; charset=utf-8');

        if (isset($result["status_code"]) && $result["status_code"] != 200
                && !array_key_exists('message',$result)) {
            $result["message"] = static::DEFAULT_STATUS_MESSAGES[$result["status_code"]];
        }

        if (isset($result["status_code"])) {
            http_response_code($result["status_code"]);
            unset($result["status_code"]);
        }

        die(json_encode($result));
    }
}?>
