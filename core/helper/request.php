<?php
namespace Helper;


class Request
{
    const HEADER_TYPES = ['AUTHORIZATION', 'APP_SECRET'];

    public function getHeaders($function_name = 'getallheaders')
    {
        $all_headers = [];

        if (function_exists($function_name)) {

            $all_headers = $function_name();
        } else {

            foreach ($_SERVER as $name => $value) {

                if (substr($name, 0, 5) == 'HTTP_') {

                    $name = substr($name, 5);
                    $name = str_replace('_', ' ', $name);
                    $name = strtolower($name);
                    $name = ucwords($name);
                    $name = str_replace(' ', '-', $name);

                    $all_headers[$name] = $value;
                } elseif ($function_name == 'apache_request_headers') {

                    $all_headers[$name] = $value;
                }
            }
        }
        return $all_headers;
    }

    /**
     *
     * @param string $headerName
     * @return string
     */
    public function getHeaderByName($headerName)
    {
        $headerName = strtoupper($headerName);

        if (!in_array($headerName, static::HEADER_TYPES)) {
            return '';
        }

        if (isset($_SERVER[$headerName])) {
            return trim($_SERVER[$headerName]);
        }

        if (isset($_SERVER['HTTP_' . $headerName])) {
            return trim($_SERVER['HTTP_' . $headerName]);
        }

        if (function_exists('apache_request_headers')) {

            $requestHeaders = apache_request_headers();
            if (isset($requestHeaders[$headerName])) {
                return trim($requestHeaders[$headerName]);
            }
        }

        return '';
    }

    public function getParams()
    {
        $params = $_GET;
        if($json = file_get_contents('php://input')){
            $data = json_decode($json);
            if(!$data){
                return [];
            }

            foreach ($data as $dKey => $dValue):
                $params[$dKey] = $dValue;
            endforeach;
        }

        return $params;
    }

    public function getBearerToken()
    {
        $headers = $this->getHeaderByName('AUTHORIZATION');
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}
