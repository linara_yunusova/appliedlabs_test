<?php
namespace Helper;


class Handler
{
    const ACCESS_HANDLER_LIST = ['authorization', 'users', 'movies',];
    private $_handler = null;

    /**
     * @var Response
     */
    public $_response;

    /**
     * @return object
     */
    public function setHandler(){
        $path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $path = rtrim($path, '/');
        $path = ltrim($path, '/');
        $separate_url = array_filter( explode('/', $path), 'strlen' );

        $response = new Response();
        if(!is_array($separate_url) || count($separate_url) <= 0){
            $response->response(["status_code" => (int) 404]);
        }

        $this->_handler = $separate_url[0];
        if(isset($separate_url[1])){
            $_GET['action'] = $separate_url[1];
        }

        if (!in_array($this->_handler, self::ACCESS_HANDLER_LIST)) {
            $response->response(["status_code" => (int) 404]);
        }

        return $this->callHandler();
    }

    public function callHandler()
    {
        $className = 'Handler\\' . ucfirst($this->_handler);
        if(class_exists($className)) {
            return new $className();
        }
        exit('error_404');
    }
}
