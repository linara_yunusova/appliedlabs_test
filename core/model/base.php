<?php
namespace Model;

use Library\Db;

abstract class Base{

    protected $_db;
    protected $_table;

    abstract function add($data);
    abstract function update($data, $id);
    abstract function getList($filter);

    public function __construct(){
        $host = getenv('DB_HOST')?getenv('DB_HOST'): '127.0.0.1';
        $username = getenv('DB_USER')?getenv('DB_USER'): 'root';
        $password = getenv('DB_PWD')?getenv('DB_PWD'): '12345678';
        $db_name = getenv('DB_NAME')?getenv('DB_NAME'): 'appliedlabs';

        $this->_db = new Db\Mysqli($host, $username, $password, $db_name);
    }

    public function getById($id){
        $query = $this->_db->query(
            "SELECT * FROM `" . $this->_table . "` WHERE id = '" . (int)$id . "'");
        return $query->row;
    }

    public function delete($id){
        return $this->_db->query("DELETE FROM `" . $this->_table . "` WHERE id = '" . (int)$id. "'");
    }

}
