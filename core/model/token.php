<?php
namespace Model;


class Token extends Base
{

    protected $_table = 'oauth2_token';

    public function add($data = []){
        return;
    }

    public function addToken($data)
    {
        $this->_db->transactionStart();

        $this->_db->query("DELETE FROM `" . $this->_table. "` WHERE `user_id` = '" . $data['user_id'] . "'");

        $this->_db->query("INSERT INTO `" . $this->_table. "` 
			SET `user_id` = '" . $data['user_id'] . "', 
			    `device_id` = '" . $data['device_id'] . "',
			    `token` = '" . $data['token'] . "',
			    `created_at` = '" . $data['created_at'] . "',
			    `expires_at` = '" . $data['expires_at'] . "'");

        if (!$this->_db->getState()){
            $this->_db->transactionRollback();
            return false;
        }

        $this->_db->transactionCommit();
        return true;
    }

    public function getByToken($token){
        $query = $this->_db->query(
            "SELECT * FROM `" . $this->_table . "` WHERE token = '" . $token . "' AND expires_at >= NOW()");
        return $query->row;
    }

    public function update($data, $id){
        return;
    }

    public function getList($filter = []){
        return;
    }
}

