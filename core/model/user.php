<?php
namespace Model;

use Model\Base;
use Helper\General;

class User extends Base
{

    protected $_table = 'user';

    public function add($data)
    {
        return;
    }

    public function update($data, $id)
    {
        return;
    }

    public function getList($filter = [])
    {
        $customer_query = $this->_db->query(
            "SELECT * FROM `" . $this->_table . "` WHERE role = 'default' ORDER by created_at DESC");
        return $customer_query->rows;
    }

    public function getUserByLogin($login){
        $query = $this->_db->query(
            "SELECT * FROM `" . $this->_table . "` 
            WHERE login = '" . strtolower($login) . "' AND is_active = True");
        return $query->row;

    }
}

