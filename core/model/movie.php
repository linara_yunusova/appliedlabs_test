<?php

namespace Model;

class Movie extends Base{

    protected $_table = 'movie';
    protected $_user_favorites_table = 'user_favorites';

    public function add($data){
        return;
    }

    public function update($data, $id){
            return;
    }

    public function getList($filter = [], $user_id = false)
    {
        $sql = "SELECT * FROM `" . $this->_table . "` WHERE is_active = 1 ";

        if(property_exists($filter, 'is_favorite') && $user_id){
            $sql .= " AND id ";
            $sql .= $filter->is_favorite==true? "IN":"NOT IN";
            $sql .= "( SELECT movie_id FROM `" . $this->_user_favorites_table . "` WHERE user_id = " . (int)$user_id . ")";
        }

        $sql .= "ORDER by created_at DESC";

        if(property_exists($filter, 'limit')){
            $sql .= " LIMIT " . (int)$filter->limit;
        }

        if(property_exists($filter, 'offset')){
            $sql .= " OFFSET " . (int)$filter->offset;
        }

        $query = $this->_db->query($sql);
        return $query->rows;
    }

    public function addUserFavorites($data)
    {
        $this->_db->query("INSERT INTO `" . $this->_user_favorites_table. "` 
			SET `user_id` = '" . $data['user_id'] . "', 
			    `movie_id` = '" . $data['movie_id'] . "'");

        return $this->_db->getState();
    }
}

