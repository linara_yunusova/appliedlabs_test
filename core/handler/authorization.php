<?php

namespace Handler;

use Helper\Request;
use Helper\Response;
use Model\Token;
use Model\User;

class Authorization{

    const APP_SECRET = 'yNDRkZWE2MMDY0OWViNWU2QyQwY';

    protected $_model;

    public function __construct(){
        $this->_response = new Response();
        $this->_request = new Request();
        $this->_params = $this->_request->getParams();

        $this->_user_model = new User();
        $this->_model = new Token();

        $this->login();
    }

    private function login(){

        $appSecret = $this->_request->getHeaderByName('APP_SECRET');
        if ($appSecret != self::APP_SECRET) {
            exit($this->_response->response(['status_code' => 400, 'message' => 'wrong secret']));
        }

        if(!array_key_exists('login', $this->_params) ||
            !array_key_exists('password', $this->_params)){
            exit($this->_response->response(['status_code' => 400, 'message' => 'wrong_credentials']));
        }

        $user = $this->_user_model->getUserByLogin($this->_params['login']);
        if(!$user){
            exit($this->_response->response(['status_code' => 400, 'message' => 'not exist user']));
        }

        // VERIFY PASSWORD
        $salt = substr($user['password'], 0, strlen($user['password']) - 32);
        $dbPassword = substr($user['password'], -32); //REVERT DB PASSWORD TO CHECK
        $userPassword = md5($salt . $this->_params['password']); // REVERT RECEIVED PASSWORD TO CHECK

        if ($dbPassword != $userPassword) {
            exit($this->_response->response(['status_code' => 400, 'message' => 'password mismatch']));
        }

        if(!$token = $this->createAuthToken($user)){
            exit($this->_response->response(['status_code' => 500]));
        }

        $result['status_code'] = 200;
        $result['response']['token'] = $token;

        return $this->_response->response($result);
    }

    private function createAuthToken($user){

        $data['user_id'] = $user['id'];
        $data['device_id'] = null;
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['expires_at'] = date('Y-m-d H:i:s', strtotime(' + 3 days'));
        $data['token'] = $this->createToken($user['login'], $data);

        if(!$this->_model->addToken($data)){
            return false;
        }

        return $data['token'];
    }

    private function randString($pass_len=10, $pass_chars=false)
    {
        static $allchars = "abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ0123456789";
        $string = "";
        if(is_array($pass_chars))
        {
            while(strlen($string) < $pass_len)
            {
                if(function_exists('shuffle'))
                    shuffle($pass_chars);
                foreach($pass_chars as $chars)
                {
                    $n = strlen($chars) - 1;
                    $string .= $chars[mt_rand(0, $n)];
                }
            }
            if(strlen($string) > count($pass_chars))
                $string = substr($string, 0, $pass_len);
        }
        else
        {
            if($pass_chars !== false)
            {
                $chars = $pass_chars;
                $n = strlen($pass_chars) - 1;
            }
            else
            {
                $chars = $allchars;
                $n = 61;
            }
            for ($i = 0; $i < $pass_len; $i++)
                $string .= $chars[mt_rand(0, $n)];
        }
        return $string;
    }

    private function createToken($login, $tokenData){
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', self::APP_SECRET);
        $exp = substr(hash('sha256', $tokenData['expires_at']), 0, 16);
        $result = openssl_encrypt($login, $encrypt_method, $key, 0, $exp);
        return base64_encode($result);
    }

    private function decodeToken($token, $tokenData){
        $encrypt_method = "AES-256-CBC";
        $exp = substr(hash('sha256', $tokenData['expires_at']), 0, 16);
        $key = hash('sha256', self::APP_SECRET);
        $exp = substr(hash('sha256', $tokenData['expires_at']), 0, 16);
        return openssl_decrypt(base64_decode($token), $encrypt_method, $key, 0, $exp);
    }

}

# GENERATE PASSWORD WHILE REGISTRATION
# $salt = $this->randString(8);
# $password = $salt.md5($salt.$password);

