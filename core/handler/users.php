<?php

namespace Handler;

use Core\Model;
use Model\User;


class Users extends Base{

    protected $_model;

    public function __construct(){
        parent::__construct();
        $this->_model = new User();
    }

    public function data(){
        $result['status_code'] = 200;
        $result['response']['list'] = $this->_model->getList();

        return $this->_response->response($result);
    }

    public function add(){
        return;
    }

}

