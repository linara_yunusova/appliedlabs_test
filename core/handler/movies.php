<?php

namespace Handler;

use Model\Movie;

class Movies extends Base{

    protected $_model;

    public function __construct(){
        parent::__construct();
        $this->_model = new Movie();
    }

    public function data(){
        $result['status_code'] = 200;

        $filter = array_key_exists('filter', $this->_params) ? $this->_params['filter'] : (object)[];
        $result['response'] = $this->_model->getList($filter, $this->_user['id']);
        return $this->_response->response($result);
    }

    public function dataFavorites(){
        $result['status_code'] = 200;

        $filter = array_key_exists('filter', $this->_params) ? $this->_params['filter'] : (object)[];
        $filter->is_favorite = true;
        $result['response'] = $this->_model->getList($filter, $this->_user['id']);

        return $this->_response->response($result);
    }

    public function addFavorites(){
        if (!array_key_exists('movie_id', $this->_params)) {
            exit($this->_response->response(['status_code' => 400]));
        }

        if(!$this->_model->getById($this->_params['movie_id'])){
            exit($this->_response->response(['status_code' => 400, 'message' => 'Movie with doesn\'t exist ']));
        }

        $data['user_id'] = $this->_user['id'];
        $data['movie_id'] = $this->_params['movie_id'];

        if(!$this->_model->addUserFavorites($data)){
            exit($this->_response->response(['status_code' => 500]));
        }

        $result['status_code'] = 200;
        $result['response'] = $this->_model->getUserFavorites($this->_user['id']);

        return $this->_response->response($result);
    }

}

