<?php
namespace Handler;

use Helper\Request;
use Helper\Response;
use Model\Token;
use Model\User;

abstract class Base{

    /**
     * @var string
     */
    public $_action;

    /**
     * @var array
     */
    public $_user;

    /**
     * @var array|[]
     */
    public $_params;

    /**
     * @var Request
     */
    public $_request;

    /**
     * @var Response
     */
    public $_response;


    /**
     * @var DB adapter
     */
    public $_db;

    public function action(){
        if(!$this->checkAction($this->_action)){
            $this->_response->response(['statusCode' => 404, 'message' => 'Method not exist']);
        }
        return $this->{$this->_action}();
    }

    /**
     * Base constructor.
     * init base class vars
     */
    public function __construct(){
        $this->_response = new Response();
        $this->_request = new Request();
        $this->_params = $this->_request->getParams();

        $this->_checkAuthorization();
        $this->_setAction();
    }

    public function _setAction(){
        $action = '';
        if (array_key_exists('action', $this->_params)) {
            $action = $this->_params['action'];
        }

        switch ($_SERVER['REQUEST_METHOD']):
            case "GET":
                $action = 'data'.ucfirst($action);
                break;
            case "POST":
                $action = 'add'.ucfirst($action);
                break;
            case "DELETE":
                $action = 'delete'.ucfirst($action);
                break;
            case "PUT":
                $action = "update".ucfirst($action);
                break;
        endswitch;

        $this->_action = $action;
    }

    private function _checkAuthorization(){
        $token = $this->_request->getBearerToken();
        if (empty($token)) {
            return $this->_response->response(["status_code" => (int) 401]);
        }
        $token_model = new Token();
        $tokenData = $token_model->getByToken($token);
        if(!$tokenData){
            return $this->_response->response(["status_code" => (int) 401]);
        }

        $this->_setUser($tokenData['user_id']);
    }

    public function _setUser($userId){
        $user_model = new User();
        if (!$userData = $user_model->getById($userId)){
            return $this->_response->response(["status_code" => (int) 401, "message" => 'Not exist user']);
        }

        $this->_user = $user_model->getById($userId);
    }

    public function checkAction($method){

        if (isset($method) && method_exists($this, $method)) {
            return true;
        }
        return false;
    }

}


