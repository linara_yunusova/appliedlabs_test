<?php
namespace Library\Db;


class Mysqli {
    private $connection;

    public function __construct($hostname, $username, $password, $database, $port = '3306') {
        if (!$this->connection = mysqli_connect($hostname, $username, $password, $database, $port)) {
            trigger_error('error database connection');
            exit();
        }
        if (!mysqli_select_db($this->connection, $database)) {
            throw new \Exception('Error: Could not connect to database ' . $database);
        }

        mysqli_query($this->connection, "SET NAMES 'utf8'");
        mysqli_query($this->connection, "SET CHARACTER SET utf8");
        mysqli_query($this->connection, "SET CHARACTER_SET_CONNECTION=utf8");
        mysqli_query($this->connection,"SET SQL_MODE = ''");
    }

    public function query($sql) {
        $query = $this->connection->query($sql);

        if (!$this->connection->errno) {
            if ($query instanceof \mysqli_result) {
                $data = array();

                while ($row = $query->fetch_assoc()) {
                    $data[] = $row;
                }

                $result = new \stdClass();
                $result->num_rows = $query->num_rows;
                $result->row = isset($data[0]) ? $data[0] : array();
                $result->rows = $data;

                $query->close();

                return $result;
            } else {
                return true;
            }
        } else {
            return false;
//            throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
        }
    }

    public function multiQuery($sql){

        $result = mysqli_multi_query($this->connection, $sql);
        //an array to store results and return at the end
        //if first query doesn't return errors
        if ($result){
            //store results of first query in the $returned array
            //set a variable to loop and assign following results to the $returned array properly
            $count = 0;
            // start doing and keep trying until the while condition below is not met
            do {
                //increase the loop count by one
                $count++;
                //go to the next result
                mysqli_next_result($this->connection);
                //get mysqli stored result for this query
                $result = mysqli_store_result($this->connection);
                //if this query in the loop doesn't return errors
                if(!$result){
                    if(!empty(mysqli_error($this->connection))){
                        throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
                    }
                }
            }
            while (mysqli_more_results($this->connection));
        }else{
            throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
        }
        return true;
    }

    public function escape($value) {
        if ($this->connection) {
            return mysqli_real_escape_string($this->connection, $value);
        }
    }

    public function countAffected() {
        if ($this->connection) {
            return mysqli_affected_rows($this->connection);
        }
    }

    public function getLastId() {
        if ($this->connection) {
            return mysqli_insert_id($this->connection);
        }
    }

    public function getState() {
        if ($this->connection) {
            return $this->connection->sqlstate == '00000';
        }
    }

    public function isConnected() {
        if ($this->connection) {
            return true;
        } else {
            return false;
        }
    }

    public function transactionStart(){
        $this->connection->begin_transaction();
    }

    public function transactionRollback(){
        $this->connection->rollback();
    }

    public function transactionCommit(){
        $this->connection->commit();
    }
    public function __destruct() {
        if ($this->connection) {
            mysqli_close($this->connection);
        }
    }
}
