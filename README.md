#Description


### Database 
<p>appliedlabs.gz</p>

### API Documentation 
<p>docs/v.1.html#/Authorization/getAuthToken</p>

### Installation
apache DocumentRoot to root folder

### Test user data for login
```json
{
	"login": "test",
	"password":"test"
}
```
 APP_SECRET = 'yNDRkZWE2MMDY0OWViNWU2QyQwY'

### Database configs
```
    $host = '127.0.0.1';
    $username = 'root';
    $password = '12345678';
    $db_name = 'appliedlabs';
```